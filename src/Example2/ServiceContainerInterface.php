<?php declare(strict_types=1);

namespace Example2;

interface ServiceContainerInterface
{
    /**
     * @param string $name
     *
     * @return mixed
     */
    public function get(string $name);

    public function set(string $name, callable $factory): ServiceContainerInterface;
}

