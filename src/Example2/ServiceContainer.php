<?php declare(strict_types=1);

namespace Example2;

class ServiceContainer implements ServiceContainerInterface
{
    /** @var array */
    private $factories = [];

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function get(string $name)
    {
        if (array_key_exists($name, $this->factories)) {
            $factory = $this->factories[$name];

            return $factory();
        }

        return null;
    }

    public function set(string $name, callable $factory): ServiceContainerInterface
    {
        $this->factories[$name] = $factory;

        return $this;
    }
}

