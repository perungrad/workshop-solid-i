<?php declare(strict_types=1);

namespace Example2\FooModule;

use Example2\ServiceContainerInterface;

class FooServiceProvider
{
    public function defineServices(ServiceContainerInterface $serviceContainer)
    {
        $serviceContainer->set('mailer', function () use ($serviceContainer) {
            return new Mailer(
                $serviceContainer->get('mailer.transport')
            );
        });

        $serviceContainer->set('mailer.transport', function () use ($serviceContainer) {
            return new MailerSmtpTransport();
        });
    }
}

