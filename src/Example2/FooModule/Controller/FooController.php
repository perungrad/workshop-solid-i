<?php declare(strict_types=1);

namespace Example2\FooModule\Controller;

use Example2\ServiceContainerInterface;

class FooController
{
    public function detailAction(ServiceContainerInterface $serviceContainer)
    {
        $repository = $serviceContainer->get('repository.user');
        $templating = $serviceContainer->get('templating');

        // do something useful
    }
}

