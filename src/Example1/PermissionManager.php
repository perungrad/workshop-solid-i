<?php declare(strict_types=1);

namespace Example1;

class PermissionManager
{
    /** @var string */
    private $user;

    /** @var string */
    private $group;

    public function __construct(string $user, string $group)
    {
        $this->user  = $user;
        $this->group = $group;
    }

    public function transferOwner(FileInterface $file)
    {
        $file->changeOwner($this>user, $this->group);
    }
}

