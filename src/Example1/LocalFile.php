<?php declare(strict_types=1);

namespace Example1;

use Example1\FileInterface;

class LocalFile implements FileInterface
{
    /** @var string */
    private $filepath;

    public function __construct(string $filepath)
    {
        $this->filepath = $filepath;
    }

    public function rename(string $name)
    {
        rename($this->filepath, $name);

        $this->filepath = $name;
    }

    public function changeOwner(string $user, string $group)
    {
        chown($this->filepath, $user);
        chgrp($this->filepath, $group);
    }
}

