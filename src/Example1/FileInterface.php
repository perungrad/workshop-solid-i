<?php declare(strict_types=1);

namespace Example1;

interface FileInterface
{
    public function rename(string $name);

    public function changeOwner(string $user, string $group);
}
