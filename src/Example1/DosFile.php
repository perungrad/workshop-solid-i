<?php declare(strict_types=1);

namespace Example1;

use Example1\FileInterface;

class DosFile implements FileInterface
{
    public function rename(string $name)
    {
        // ...
    }

    public function changeOwner(string $user, string $group)
    {
        throw new \BadMethodCallException(
            'Not implemented for Dropbox files'
        );
    }
}

