<?php declare(strict_types=1);

namespace Example3\Project\Database;

interface ConnectionInterface
{
    public function select(string $columns): ConnectionInterface;

    public function from(string $table): ConnectionInterface;

    public function where(string $condition): ConnectionInterface;

    public function fetchAll(): array;
}
