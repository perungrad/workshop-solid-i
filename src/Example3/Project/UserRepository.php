<?php declare(strict_types=1);

namespace Example3\Project;

use Example3\Project\Database\ConnectionInterface;

class UserRepository
{
    /** @var ConnectionInterface */
    private $connection;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    public function getActiveUsers(): array
    {
        return $this->connection->select('id, name, last_active_at')
            ->from('user')
            ->where('is_active = 1')
            ->fetchAll();
    }
}
