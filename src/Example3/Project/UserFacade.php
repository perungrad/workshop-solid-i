<?php declare(strict_types=1);

namespace Example3\Project;

use Example3\Project\UserRepository;
use Example3\Library\ArrayConverter;

class UserFacade
{
    /** @var UserRepository */
    private $userRepository;

    /** @var ArrayConverter */
    private $arrayConverter;

    public function __construct(UserRepository $userRepository, ArrayConverter $arrayConverter)
    {
        $this->userRepository = $userRepository;
        $this->arrayConverter = $arrayConverter;
    }

    public function getActiveUsers(): array
    {
        $usersData = $this->userRepository->getActiveUsers();

        return $this->arrayConverter->convert($usersData);
    }
}
