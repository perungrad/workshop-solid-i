<?php declare(strict_types=1);

namespace Example3\Library;

class ArrayConverter
{
    public function convert(array $input): array
    {
        $result = [];

        foreach ($input as $key => $data) {
            if (is_array($data)) {
                $data = $this->convert($data);
            }

            if ($data instanceof \DateTime) {
                $data = $data->format('Y-m-d H:i:s');
            }

            $result[$this->convertUndescoreToCamelCase($key)] = $data;
        }

        return $result;
    }

    private function convertUndescoreToCamelCase(string $str): string
    {
        $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $str)));

        $str[0] = strtolower($str[0]);

        return $str;
    }
}
