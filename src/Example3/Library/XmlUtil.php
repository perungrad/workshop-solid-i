<?php declare(strict_types=1);

namespace Example3\Library;

use Example3\Library\ArrayConverter;

class XmlUtil
{
    /** @var ArrayConverter */
    private $arrayConverter;

    public function __construct(ArrayConverter $arrayConverter)
    {
        $this->arrayConverter = $arrayConverter;
    }

    public function formatTagNameToCamelCase(string $tagName): string
    {
        return $this->arrayConverter->convertUndescoreToCamelCase($tagName);
    }
}
